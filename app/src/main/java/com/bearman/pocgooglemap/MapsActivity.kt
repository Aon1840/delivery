package com.bearman.pocgooglemap

import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {

    private var placesClient: PlacesClient? = null
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lastLocation: Location? = null
    private var locationMarker: Marker? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        Places.initialize(this, "AIzaSyAvHhquqVwZRd2vtUWzwTayiLsWyjWmK2w")
        placesClient = Places.createClient(this)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initView()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)

        requestPermission()

    }

    private fun setUpMap() {
        map.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(13.736717, 100.523186)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
            }
        }

        map.setOnMapClickListener {
            map.clear()
            val marker = MarkerOptions().apply {
                position(it)
                title(getCurrentLocationName())
                snippet("Address")
            }
            locationMarker = map.addMarker(marker)
            locationMarker?.isDraggable = true
            locationMarker?.showInfoWindow()
        }
    }

    private fun initOnmapChange() {
        map.setOnCameraMoveListener {
            Handler().postDelayed({
                locationMarker?.remove()
                val center = map.cameraPosition.target
                lastLocation?.latitude = center.latitude
                lastLocation?.longitude = center.longitude
                val marker = MarkerOptions().apply {
                    position(center)
                    title(getCurrentLocationName())
                    snippet("Address")
                }
                locationMarker = map.addMarker(marker)
                locationMarker?.isDraggable = true
                locationMarker?.showInfoWindow()
            }, 2000)
        }
    }

    private fun getCurrentLocationName(): String {
        val geoCoder = Geocoder(applicationContext, Locale.getDefault())
        var lat = 0.00
        var long = 0.00
        lastLocation?.let {
            lat = it.latitude
            long = it.longitude
        }
        val address = geoCoder.getFromLocation(lat, long, 1)
        return try {
            address[0].getAddressLine(0).toString()
        } catch (e: Exception) {
            ""
        }
    }

    override fun onMarkerClick(p0: Marker?) = false

    private fun requestPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    setUpMap()
                } else {
                    Toast.makeText(this, "permission denied, boo! Disable the", Toast.LENGTH_LONG)
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                Toast.makeText(this, "Ignore all other requests", Toast.LENGTH_LONG)

                // Ignore all other requests.
            }
        }
    }

    private fun initView() {
        bt_add_mark.setOnClickListener {
            locationMarker?.remove()
            val center = map.cameraPosition.target
            lastLocation?.latitude = center.latitude
            lastLocation?.longitude = center.longitude
            val marker = MarkerOptions().apply {
                position(center)
                title(getCurrentLocationName())
                snippet("Address")
            }
            locationMarker = map.addMarker(marker)
            locationMarker?.isDraggable = true
            locationMarker?.showInfoWindow()
        }
//        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextChange(newText: String): Boolean {
//                return false
//            }
//
//            override fun onQueryTextSubmit(query: String): Boolean {
//                val location = search_view.query.toString()
//                if (location.isNotBlank()) {
//                    val geocoder = Geocoder(this@MapsActivity)
//                    val address = geocoder.getFromLocationName(location, 5)
//                    try {
//                        lastLocation?.let {
//                            it.latitude = address[0].latitude
//                            it.longitude = address[0].longitude
//                        }
//                        val latLng = LatLng(address[0].latitude, address[0].longitude)
//                        val marker = MarkerOptions().apply {
//                            position(latLng)
//                            title(getCurrentLocationName())
//                            snippet("Address")
//                        }
//                        locationMarker = map.addMarker(marker)
//                        locationMarker?.isDraggable = true
//                        locationMarker?.showInfoWindow()
//                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
//                    } catch (e: Exception) {
//
//                    }
//                }
//                return false
//            }
//        })

        searchLocation()
    }

    private fun searchLocation() {
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.fragment) as AutocompleteSupportFragment?

        autocompleteFragment!!.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME))
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) { // TODO: Get info about the selected place.
                Log.i("TAG", "Place: " + place.name + ", " + place.id)
                val geocoder = Geocoder(this@MapsActivity)
                val address = geocoder.getFromLocationName(place.name, 5)
                lastLocation?.let {
                    it.latitude = address[0].latitude
                    it.longitude = address[0].longitude
                }
                val latLng = LatLng(address[0].latitude, address[0].longitude)
                val marker = MarkerOptions().apply {
                    position(latLng)
                    title(getCurrentLocationName())
                    snippet("Address")
                }
                locationMarker = map.addMarker(marker)
                locationMarker?.isDraggable = true
                locationMarker?.showInfoWindow()
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                selectLocation(getCurrentLocationName())
            }

            override fun onError(p0: Status) {
                Log.i("TAG", "An error occurred: $p0")
            }
        })
    }

    private fun selectLocation(address: String) {
        map.setOnInfoWindowClickListener {
            startActivity(LandingActivity.newInstance(this, address))
//            val startString = "${lastLocation?.latitude},${lastLocation?.longitude}"
//            val destString = "${it.position.latitude},${it.position.longitude}"
//
//            val url = "http://maps.google.com/maps?saddr=$startString&daddr=$destString"
//
//            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
//            startActivity(intent)
        }
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

}
