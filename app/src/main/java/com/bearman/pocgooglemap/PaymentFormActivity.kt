package com.bearman.pocgooglemap

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import co.omise.android.api.Client
import co.omise.android.api.Request
import co.omise.android.api.RequestListener
import co.omise.android.models.CardParam
import co.omise.android.models.Token
import kotlinx.android.synthetic.main.activity_payment_form.*

class PaymentFormActivity : AppCompatActivity() {

    private lateinit var client: Client

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_form)
        title = "Credit Card Form"
        client = Client("pkey_test_5jkz7dqvwmw2vlm7u45")
        initView()
    }

    private fun initView() {
        bt_payment.apply {
            setOnClickListener {
                val cardParam = CardParam(
                    et_card_name.cardName,
                    et_card_no.cardNumber,
                    et_expire_date.expiryMonth,
                    et_expire_date.expiryYear,
                    et_secure_code.securityCode
                )
                val request = Token.CreateTokenRequestBuilder(cardParam).build()
                callCreditCardService(request)
            }
        }
    }

    private fun callCreditCardService(request: Request<Token>) {
        client.send(request, object : RequestListener<Token> {
            override fun onRequestFailed(throwable: Throwable) {
                Log.d("Error", throwable.localizedMessage)
            }

            override fun onRequestSucceed(model: Token) {
                showDialog(model.toString())
                Log.d("Error", model.modelObject)

            }

        })
    }

    private fun showDialog(message: String) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
            finish()
        })
        builder.show();
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent {
            return Intent(context, PaymentFormActivity::class.java)
        }
    }
}