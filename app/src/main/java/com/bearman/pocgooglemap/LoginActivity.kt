package com.bearman.pocgooglemap

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_login.*
import java.net.URL

class LoginActivity : AppCompatActivity() {

    private val callbackManager = CallbackManager.Factory.create()
    private val loginFbManager = LoginManager.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun initView() {
        loginFbManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) { // App code
                getAccessToken(loginResult)
                navigateToMainActivity()
                Log.e("onSuccess", "onSuccess")
            }

            override fun onCancel() { // App code
                Log.e("onCancel", "onCancel")

            }

            override fun onError(exception: FacebookException) { // App code
                Log.e("onError", "onError")
            }
        })

        bt_skip.setOnClickListener {
            navigateToMainActivity()
        }

        bt_otp.setOnClickListener {
            startActivity(OTPActivity.newIntent(this))
        }
    }

    private fun navigateToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun getAccessToken(loginResult: LoginResult?) {
        val request = GraphRequest.newMeRequest(loginResult?.accessToken) { data, response ->
            val id = data.optString("id")
            val name = data.optString("first_name") + data.optString("last_name")
            val email = data.optString("email")
            Log.e("id", id)
            Log.e("name", name)
            Log.e("email", email)
            val profile_pic =
                URL("https://graph.facebook.com/$id/picture?width=200&height=150")
            Log.e("profile_pic", profile_pic.toString())
        }
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id, first_name, last_name, email,gender, birthday, location"
        )
        request.parameters = parameters
        request.executeAsync()
    }
}
