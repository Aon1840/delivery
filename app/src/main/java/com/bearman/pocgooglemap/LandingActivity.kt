package com.bearman.pocgooglemap

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class LandingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)
        intent.extras?.let { extra ->
            title = extra.getString(EXTRA_ADDRESS)
        }
    }

    companion object {
        private const val EXTRA_ADDRESS = "EXTRA_ADDRESS"

        @JvmStatic
        fun newInstance(context: Context, address: String): Intent {
            return Intent(context, LandingActivity::class.java).apply {
                putExtra(EXTRA_ADDRESS, address)
            }
        }
    }
}