package com.bearman.pocgooglemap

import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_map_search.*

class MapSearchActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private val sydney = LatLng((-34).toDouble(), (151).toDouble())
    private val myHome = LatLng(-34.1, (150).toDouble())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_search)

        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        bt_open_google_map.setOnClickListener {
            openGoogleMap(myHome, sydney)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        setStyleGoogleMap(googleMap)

        // Add a marker
        mMap = googleMap
        mMap.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney")
                .snippet("Hello Sydney")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_food))
                .rotation(20f)
                .draggable(false)
        )
        mMap.addMarker(
            MarkerOptions()
                .position(myHome)
                .title("My Home")
                .snippet("Hello my home")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_food))
                .rotation(20f)
                .draggable(false)
        )
        val center = LatLng(-34.05, (150.5).toDouble())
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 8f))

        // Set image
        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoWindow(arg0: Marker): View? {
                return null
            }

            override fun getInfoContents(marker: Marker): View { // Inflate the layouts for the info window, title and snippet.
                val infoWindow: View =
                    layoutInflater.inflate(R.layout.custom_info_contents, null)
                val title = infoWindow.findViewById(R.id.textViewName) as TextView
                title.text = marker.title
                val snippet = infoWindow.findViewById(R.id.textViewSnippet) as TextView
                snippet.text = marker.snippet
                val imageView: ImageView = infoWindow.findViewById(R.id.imageView) as ImageView
                imageView.setImageResource(R.drawable.ic_building)
                if ("My Home" == marker.title) {
                    imageView.setImageResource(R.drawable.ic_home)
                }
                return infoWindow
            }
        })

        mMap.addPolyline(
            PolylineOptions().geodesic(true)
                .add(sydney)
                .add(myHome)
        )

    }

    private fun setStyleGoogleMap(googleMap: GoogleMap) {
        try {
            val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.style_json
                )
            )
            if (!success) {
                Log.e("test", "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e("test", "Can't find style. Error: ", e)
        }
    }

    private fun openGoogleMap(src: LatLng, dest: LatLng) {
        val url =
            "http://maps.google.com/maps?saddr=" + src.latitude + "," + src.longitude + "&daddr=" + dest.latitude + "," + dest.longitude + "&mode=driving"
        val gmmIntentUri: Uri = Uri.parse(url)
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
