package com.bearman.pocgooglemap

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_otp.*


class OTPActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        initView()
    }

    private fun initView() {
//        et_otp1.addTextChangedListener(GenericTextWatcher(et_otp1))
//        et_otp2.addTextChangedListener(GenericTextWatcher(et_otp2))
//        et_otp3.addTextChangedListener(GenericTextWatcher(et_otp3))
//        et_otp4.addTextChangedListener(GenericTextWatcher(et_otp4))
//        et_otp5.addTextChangedListener(GenericTextWatcher(et_otp5))
//        et_otp6.addTextChangedListener(GenericTextWatcher(et_otp6))

        et_otp1.addTextChangedListener(GenericTextWatcher(et_otp2, et_otp1))
        et_otp2.addTextChangedListener(GenericTextWatcher(et_otp3, et_otp1))
        et_otp3.addTextChangedListener(GenericTextWatcher(et_otp4, et_otp2))
        et_otp4.addTextChangedListener(GenericTextWatcher(et_otp5, et_otp3))
        et_otp5.addTextChangedListener(GenericTextWatcher(et_otp6, et_otp4))
        et_otp6.addTextChangedListener(GenericTextWatcher(et_otp6, et_otp5))

//        et_otp1.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 1) {
//                    et_otp2.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
//        et_otp2.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 1) {
//                    et_otp3.requestFocus()
//                } else {
//                    et_otp1.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
//        et_otp3.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 1) {
//                    et_otp4.requestFocus()
//                } else {
//                    et_otp2.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
//        et_otp4.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 1) {
//                    et_otp5.requestFocus()
//                } else {
//                    et_otp3.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
//        et_otp5.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 1) {
//                    et_otp6.requestFocus()
//                } else {
//                    et_otp4.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
//        et_otp6.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.length == 0) {
//                    et_otp5.requestFocus()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })
    }

    class GenericTextWatcher(private val etNext: EditText, private val etPrev: EditText) :
        TextWatcher {

        override fun afterTextChanged(editable: Editable) {
            val text = editable.toString()
            if (text.length == 1) etNext.requestFocus() else if (text.length == 0) etPrev.requestFocus()
        }

        override fun beforeTextChanged(
            arg0: CharSequence,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) {
        }

        override fun onTextChanged(
            arg0: CharSequence,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) {
        }

    }

//    class GenericTextWatcher(private val view: View) : TextWatcher {
//        override fun afterTextChanged(editable: Editable) { // TODO Auto-generated method stub
//            val text = editable.toString()
//            when (view.id) {
//                view.et_otp1.id -> {
//                    if (text.length == 1) {
//                        (view.et_otp2 as EditText).requestFocus()
////                        view.et_otp2.requestFocus()
//                    }
//                }
//                view.et_otp2.id -> if (text.length == 1) view.et_otp3.requestFocus() else if (text.isEmpty()) view.et_otp1.requestFocus()
//                view.et_otp3.id -> if (text.length == 1) view.et_otp4.requestFocus() else if (text.isEmpty()) view.et_otp2.requestFocus()
//                view.et_otp4.id -> if (text.length == 1) view.et_otp5.requestFocus() else if (text.isEmpty()) view.et_otp3.requestFocus()
//                view.et_otp5.id -> if (text.length == 1) view.et_otp6.requestFocus() else if (text.isEmpty()) view.et_otp4.requestFocus()
//                view.et_otp6.id -> if (text.isEmpty()) view.et_otp5.requestFocus()
//            }
//        }
//
//        override fun beforeTextChanged(
//            arg0: CharSequence,
//            arg1: Int,
//            arg2: Int,
//            arg3: Int
//        ) { // TODO Auto-generated method stub
//        }
//
//        override fun onTextChanged(
//            arg0: CharSequence,
//            arg1: Int,
//            arg2: Int,
//            arg3: Int
//        ) { // TODO Auto-generated method stub
//        }
//

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent {
            return Intent(context, OTPActivity::class.java)
        }
    }
}