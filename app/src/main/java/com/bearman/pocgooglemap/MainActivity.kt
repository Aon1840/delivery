package com.bearman.pocgooglemap

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.chip.Chip
import com.google.android.material.shape.ShapeAppearanceModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        bt_google_map.setOnClickListener {
            val intent = Intent(this, MapSearchActivity::class.java)
            startActivity(intent)
        }

        bt_google_map2.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            startActivity(intent)
        }

        bt_payment.setOnClickListener {
            startActivity(PaymentFormActivity.newIntent(this))
        }
        setupUpNavigationView()

        chip_group.setChipSpacing(2)
        val listText =
            listOf("aaaaaa", "bbbbbb", "ccccccsaasfasf;las", "asdjaspdjas;fjo;", "sdfsdlfhdsljfl")
        for (item in listText) {
            val layoutInflater = LayoutInflater.from(this)
            val chip = layoutInflater.inflate(R.layout.chip_item, null, false) as Chip
            chip.text = item
            chip_group.addView(chip)
        }
    }

    private fun setupUpNavigationView() {
        mBottomNavigationView.setOnTabSelectListener { tabId: Int ->
            onTabSelection(tabId)
        }
        mBottomNavigationView.setDefaultTab(R.id.account_tab)
    }

    private fun onTabSelection(tabId: Int) {
        when (tabId) {
            R.id.search_tab -> {
                Toast.makeText(applicationContext, "search_tab", Toast.LENGTH_SHORT).show()
            }
            R.id.filter_tab -> {
                Toast.makeText(applicationContext, "filter_tab", Toast.LENGTH_SHORT).show()
            }
            R.id.favourite_tab -> {
                Toast.makeText(applicationContext, "favourite_tab", Toast.LENGTH_SHORT).show()
            }
            R.id.account_tab -> {
                Toast.makeText(applicationContext, "account_tab", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun checkRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.rb_payment ->
                    if (checked) {
                        rb_payment.setTextColor(Color.WHITE)
                        rb_map.setTextColor(Color.RED)
                    }
                R.id.rb_map ->
                    if (checked) {
                        rb_payment.setTextColor(Color.RED)
                        rb_map.setTextColor(Color.WHITE)
                    }
            }
        }
    }
}
